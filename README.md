![Membrete Batoi](md_media/css/MembreteBatoi.png)

# Examen Práctico de PSP - DAM A - Ataque a los Titanes

En el anime de *Ataque a los Titanes* que es una serie de animación japonesa. El hilo principal de la serie es que existen unos seres enormes de unos 10 metros de altura media, con muy poca inteligencia, que se alimentan exclusivamente de humanos. A estos seres la humanidad los denominó Titanes.

Para defenderse de ellos, se creó un cuerpo militar llamado *Ejército de Paradis*. Para ser miembros de este ejército se tiene que pasar todo un programa de formación militar. Los 10 mejores miembros de la tropa de reclutas que se graduó en la promoción 104 está formada por los siguientes personajes (Adjunto a cada personaje puede leerse la evaluación individual):

1. **Mikasa Ackerman** - Una experta en todas las áreas. Su talento no tiene precedentes y es considerada una de las mejores alumnas de nuestra historia.
2. **Reiner Braun** - Posee una gran fortaleza mental y física, sus compañeros confían profundamente en él.
3. **Bertolt Hoover** - Posee un gran potencial, pero le hace falta iniciativa.
4. **Annie Leonhart** - No tiene defectos en el ángulo de ataque pero tiene problemas con el trabajo en equipo. Es un lobo solitario.
5. **Eren Jaeger** - Aunque carece de algún talento excepcional, mejoró sus calificaciones a través de una gran perseverancia, además posee una excepcional determinación
6. **Jean Kirstein** - Su habilidad del dominio periférico es de primera clase pero su personalidad causa fricción en el equipo.
7. **Marco Bott** - Es un compañero muy hábil en la gestión de equipos.
8. **Connie Springer** - Es un experto en hacer movimientos bruscos, pero su mente no es lo suficientemente aguda.
9. **Sasha Blouse** - Es tan veloz como el rayo. Pero su excentricidad la hace no apta para las operaciones en grupo.
10. **Krista Lenz** - Aunque no ha demostrado aún grandes habilidades en el combate, muestra signos de liderazgo. 

La misión de estos soldados es proteger a la humanidad y su labor consiste en acabar con los titanes que 
intentan comerse a los humanos.

Estos soldados están equipados con el *Equipo de Maniobras Tridimensional*, que les permite desplazarse a una
gran velocidad y les proporciona todo lo necesario para acabar con los titanes.

Para ayudar al comandante *Armin Arlert* del *Cuerpo de Exploración* a planificar las misiones, realiza las siguientes simulaciones:

## 1. **EJERCICIO 1** ( 2p )

Realiza un programa que simule una misión de estos personajes en la que tienen que desplazarse 10 Km en intervalos de 500 metros y descansando entre 50 y 100 ms. Como se está simulando una misión peligrosa, hay un 75% de  probabilidades de que en cada intervalo exterminen a un titan. Para este ejercicio, no deben utilizarse clases anidadas. Cada hilo debería llamarse como el personaje para poderlo identificar.

Imprime los siguientes mensajes:

* Durante la ejecución, cada personaje ha de imprimir su nombre en cada intervalo la distancia recorrida total y si ha matado o no a un titán.
* Al final de la misión, se ha de mostrar cuantos titanes ha matado cada personaje.

En el archivo [output_1.txt](md_media/output_1.txt) puede verse un ejemplo de ejecución. En esta ejecución se muestra un mensaje al finalizar la ejecución pero para solucionar este ejercicio no es necesaria.

## 2. **EJERCICIO 2** ( 2p )

Crea una nueva versión del ejercicio anterior en la que se almacene el número de titanes con los que ha acabado el equipo y imprímela cuando la misión haya finalizado, es decir, cuando todos los personajes hayan recorrido
la distancia. ( Esta información se ha de imprimir después de la información final de los personajes )

En el archivo [output_2.txt](md_media/output_2.txt) puede verse un ejemplo de la salida.

## 3. **EJERCICIO 3** ( 3p )

En la serie, existen unos titanes especiales que son inteligentes. Uno de ellos es el **Titan Acorazado**, uno de los titanes más temidos por el *Ejército de Paradis*.

Si los otros titanes tienen una probabilidad de un 0,1% de matar a un personaje de esta promoción durante un intervalo, éste tiene una probabilidad de un 10% de matar a uno de los personajes.  

Para realizar este ejercicio, introduce al **Titán Acorazado** en la simulación de forma que cada 25ms intente matar a un personaje. Esto lo realizará durante 30 veces. Un personaje muerto, finaliza su ejecución y no acaba la misión

Muestra los siguientes mensajes:

* Un mensaje cada vez que el titan se active y si ha matado a un personaje. Los personajes muestran un mensaje como que se les ha matado.

En el archivo [output_3.txt](md_media/output_3.txt) puede verse un ejemplo de ejecución. Los mensajes del Titán Colosal se identifican con el prefijo **TA:**

## 4. Bibliografía

1. [Ataque a los Titanes Fandom](https://shingeki-no-kyojin.fandom.com/es/wiki/Shingeki_no_Kyojin_Wiki)
2. [El logo del proyecto descargado tiene licencia para uso no comercial](https://www.pngwing.com/es/free-png-zhdhu/download?width=100)
