import java.util.concurrent.ThreadLocalRandom;

public class Soldado implements Runnable{

    
    String nombre;
    float distancia_total;
    float distancia_intervalo;
    int ms_min_descanso;
    int ms_max_descanso;
    float prob_matar_titan;
    ContadorTitanes contador_titanes;


    public Soldado(String nombre, float distancia_total, float distancia_intervalo, int ms_min_descanso,
            int ms_max_descanso, float prob_matar_titan, ContadorTitanes contador_titanes) {
        this.nombre = nombre;
        this.distancia_total = distancia_total;
        this.distancia_intervalo = distancia_intervalo;
        this.ms_min_descanso = ms_min_descanso;
        this.ms_max_descanso = ms_max_descanso;
        this.prob_matar_titan = prob_matar_titan;
        this.contador_titanes = contador_titanes;
    }

    public Soldado(String nombre, float distancia_total, float distancia_intervalo, int ms_min_descanso,
            int ms_max_descanso, float prob_matar_titan) {
        this(nombre,distancia_total,distancia_intervalo,ms_min_descanso,ms_max_descanso,prob_matar_titan,null);
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public void run() {

        int titanes_muertos = 0;
    
        for (int i = (int)distancia_intervalo; i <= distancia_total; i+=distancia_intervalo){

            boolean mata_titan = ThreadLocalRandom.current().nextInt(0, 100) < prob_matar_titan*100;
            int ms_descanso = ThreadLocalRandom.current().nextInt(ms_min_descanso,ms_max_descanso);

            try {

                Thread.sleep(ms_descanso);

                if (!mata_titan){
                    
                    System.out.println(nombre + "\t\tha recorrido " + i + "m y no ha matado ningun titán");
                
                } else {

                    titanes_muertos++;

                    if (contador_titanes != null){
                        contador_titanes.incrementarContador();
                    }

                    System.out.println(nombre + "\t\tha recorrido " + i + "m y ha matado 1 titán");

                }

            } catch (Exception e) {
                
            }
            
        }

        System.out.println(nombre + "\t\tMISION FINALIZADA, ha exterminado un total de "+titanes_muertos + " titanes.");
        
    }

    

    
}
