public class ContadorTitanes {

    int titanes_muertos;

    public int getTitanesMuertos() {
        return titanes_muertos;
    }

    public ContadorTitanes(){

        titanes_muertos = 0;

    }

    public synchronized void incrementarContador()
    {
        titanes_muertos++;
    }
    
}
