import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TitanAcorazado implements Runnable {

    List<Thread> soldados_vivos = new ArrayList<>();

    public TitanAcorazado(Thread[] soldados) {

        for (int i = 0; i < soldados.length; i++) {
            soldados_vivos.add(soldados[i]);
        }

    }

    @Override
    public void run() {
        
        for (int i = 0; i < 30; i++) {

            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            boolean mata_soldado = ThreadLocalRandom.current().nextInt(0, 100) < 10; //10%

            if ( mata_soldado ) {

                if (soldados_vivos.size() != 0){

                    //Intentamos matar el primer soldado
                    Thread soldado_a_matar = soldados_vivos.get(0);

                    if ( soldado_a_matar.isAlive()){
                        soldado_a_matar.interrupt();
                        soldados_vivos.remove(soldado_a_matar);
                    }

                    System.out.println("TA: ¡He matado a un soldado!");

                } else {

                    System.out.println("TA: ¡No quedan soldados vivos!");
                    return;

                }

            } else {

                System.out.println("TA: ¡No he podido matar a ningún soldado!");

            }

        }
        
    }
    
}
