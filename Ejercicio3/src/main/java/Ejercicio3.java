public class Ejercicio3 {

    public static void main(String args[] ){
        //Personajes
        String[] personajes = 
        new String[]{
            "Mikasa Ackerman",
            "Reiner Braun",
            "Bertolt Hoover",
            "Annie Leonhart", 
            "Eren Jaeger", 
            "Jean Kirstein", 
            "Marco Bott",
            "Connie Springer", 
            "Sasha Blouse",
            "Krista Lenz" 
        };

        Soldado[] soldados = new Soldado[personajes.length];

        int distancia_total = 10000;
        int distancia_intervalo = 500;
        int ms_min_descanso = 50;
        int ms_max_descanso = 100;
        float prob_matar_titan = 0.75f;

        ContadorTitanes titanes_muertos = new ContadorTitanes();

        for (int i = 0; i<personajes.length;i++){

            soldados[i] = new Soldado(personajes[i], 
                                distancia_total, 
                                distancia_intervalo, 
                                ms_min_descanso, 
                                ms_max_descanso,
                                prob_matar_titan,
                                titanes_muertos);

        }

        Thread threads[] = new Thread[soldados.length];         

        for (int i = 0; i < threads.length; i++) {

            threads[i] = new Thread(soldados[i],soldados[i].getNombre());

        }

        Thread titanAcorazado = new Thread( new TitanAcorazado(threads) , "Titán Acorazado" );

        System.out.println("============ Inicio de la Simulación ============");

        for (int i = 0; i < threads.length; i++) {

            threads[i].start();

        }

        titanAcorazado.start();

        for (int i = 0; i < threads.length; i++) {

            try {
                threads[i].join();
            } catch (InterruptedException e) {
                System.err.println(e.getLocalizedMessage());
            }

        }

        try {

            titanAcorazado.join();

        } catch (InterruptedException e) {
            
            e.printStackTrace();
            
        }

        System.out.println("===");
        System.out.println("EL EQUIPO\t\tHa matado un total de: " + titanes_muertos.getTitanesMuertos() + " Titanes");
        System.out.println("===");
        System.out.println("============ Simulación Finalizada ============");


    }

}
